import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  CheckIcon = faCheck;
  Fname =  new FormControl('');
  Lname = new FormControl('');
  emailId = new FormControl('');
  city = new FormControl('');

  Pass = new FormGroup({
    Password: new FormControl(''),
    ConfirmPass: new FormControl('')
  });

  Gender = new FormGroup({
    Female: new FormControl(''),
    Male: new FormControl(''),
    Other: new FormControl(''),
  });
  constructor() { }

  ngOnInit(): void {
  }
  PasswordValid(): boolean{
    if (this.Pass.get('ConfirmPass') !== this.Pass.get('Password')){
      return false
    }
  }




}
